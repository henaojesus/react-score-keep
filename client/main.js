import React from 'react';
import ReactDOM from 'react-dom';
import { Meteor } from 'meteor/meteor';
import { Tracker } from 'meteor/tracker';

//Mongo Collection
import { Players, calculatePlayerPositions } from './../imports/api/players';
//UI Components
import App from '../imports/ui/App';

/* Meteor.startup is called when the DOM is ready */
Meteor.startup(() => {
  //Tracker Listens changes in a collection
  Tracker.autorun(() => {
    /* Get MongoCollection with no conditions, sorted descending score */
    let players = Players.find({}, { sort: { score: -1 } }).fetch();
    let positionedPlayers = calculatePlayerPositions(players);
    /* Get MongoCollection where name is equal to Jesus, sorted ascending score 
    let players = Players.find({name: 'Jesus'}, { sort: { score: 1 } }).fetch(); */
    let title = 'Score Keep';
    ReactDOM.render(<App title={title} players={positionedPlayers} />, document.getElementById('app'));
  });
});