import { Mongo } from 'meteor/mongo';
import numeral from 'numeral';

//Constructor Function: Creates mongo and miniMongo Collection
export const Players = new Mongo.Collection('players');

Players.allow({
  insert() { return true; },
  update() { return true; },
  remove() { return true; }
});

export const calculatePlayerPositions = (players) => {
  let rank = 1;

  return players.map((player, index) => {
    if (index !== 0 && players[index - 1].score > player.score) {
      renk++;
    }
    return {
      ...player,
      rank,
      position: numeral(rank).format('0o')
    }
  });
}