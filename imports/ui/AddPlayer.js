import React from 'react';

/* Mongo Collections */
import { Players } from './../api/players';

export default class AddPlayer extends React.Component {
  handleSubmit(event) {
    event.preventDefault();
    let playerName = event.target.playerName.value;
    if (playerName) {
      //Clear Input
      event.target.playerName.value = '';
      //Insert player into Player miniMongo Collection
      Players.insert({
        //_id is generated randomly by meteor
        name: playerName,
        score: 0
      });
    }
  }

  render() {
    return (
      <div className="item">
        <form className="form" onSubmit={this.handleSubmit.bind(this)}>
          <input className="form__input" type="text" name="playerName" placeholder="Player name" />
          <button className='button'>Add Player</button>
        </form>
      </div>
    );
  }
}

