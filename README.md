# README #

Score Keeping tutorial app on React and Meteor

### What is this repository for? ###

* Quick summary: 
React, Meteor and Mongodb app for a Score Keeping app
* Version 1

### How do I get set up? ###

* Summary of set up

* 1. Install node
* 2. NPM Install for dependencies	
* 3. Meteor add ecmascript, Meteor add mongo, meteor add static-html

* Package Dependencies
* fourseven:scss
* react-flip-move

* Database configuration: Mongo.Collection('players')

* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact